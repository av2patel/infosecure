//
//  Website.h
//  infoSecure
//
//  Created by Amish Patel on 2013-03-25.
//  Copyright (c) 2013 Amish Patel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Website : NSManagedObject

@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSString * webSiteDescription;
@property (nonatomic, retain) NSString * websiteTitle;

@end
