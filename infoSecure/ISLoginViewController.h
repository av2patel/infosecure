//
//  ISLoginViewController.h
//  infoSecure
//
//  Created by Amish Patel on 2013-03-25.
//  Copyright (c) 2013 Amish Patel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISLoginViewController : UIViewController

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) IBOutlet UITextField *usernameField;
@property (strong, nonatomic) IBOutlet UITextField *passwordField;


@end
