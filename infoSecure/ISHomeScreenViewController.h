//
//  ISHomeScreenViewController.h
//  infoSecure
//
//  Created by Amish Patel on 2013-03-19.
//  Copyright (c) 2013 Amish Patel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ISContactsCell.h"
#import "ISNotesCell.h"
#import "ISPicturesCell.h"
#import "ISThingsToDoCell.h"
#import "ISWebsitesCell.h"
#import "ISYoutubeCell.h"
#import "ISTableViewSeparatorCell.h"
#import "ISMiscellaneousCell.h"


@interface ISHomeScreenViewController : UIViewController

@property (strong, nonatomic) NSArray *titles;

@end
