//
//  Website.m
//  infoSecure
//
//  Created by Amish Patel on 2013-03-25.
//  Copyright (c) 2013 Amish Patel. All rights reserved.
//

#import "Website.h"


@implementation Website

@dynamic url;
@dynamic webSiteDescription;
@dynamic websiteTitle;

@end
