//
//  Youtube.h
//  infoSecure
//
//  Created by Amish Patel on 2013-03-25.
//  Copyright (c) 2013 Amish Patel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Youtube : NSManagedObject

@property (nonatomic, retain) NSString * embeddedUrl;
@property (nonatomic, retain) NSString * youtubeDescription;
@property (nonatomic, retain) NSString * youtubeTitle;

@end
