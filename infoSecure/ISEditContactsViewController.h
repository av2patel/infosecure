//
//  ISEditContactsViewController.h
//  infoSecure
//
//  Created by Amish Patel on 2013-04-10.
//  Copyright (c) 2013 Amish Patel. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ISEditContactViewControllerDelegate <NSObject>

-(void)reloadObjectInfo;

@end

@interface ISEditContactsViewController : UIViewController<UITextFieldDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate>


@property (nonatomic, strong) NSManagedObject *managedObject;
- (IBAction)donePressed:(UIBarButtonItem *)sender;
- (IBAction)cancelPressed:(UIBarButtonItem *)sender;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *cellTextField;
@property (weak, nonatomic) IBOutlet UITextField *homeTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *occupationTextField;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) UIImagePickerController *picker;
@property (strong, nonatomic) UIImagePickerController *picker2;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

- (IBAction)pickImageFromCamera:(UIButton *)sender;
- (IBAction)pickImageFromGallary:(UIButton *)sender;

@end
