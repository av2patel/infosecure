//
//  ISTableViewSeparatorCell.m
//  infoSecure
//
//  Created by Amish Patel on 2013-03-21.
//  Copyright (c) 2013 Amish Patel. All rights reserved.
//

#import "ISTableViewSeparatorCell.h"

@implementation ISTableViewSeparatorCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
