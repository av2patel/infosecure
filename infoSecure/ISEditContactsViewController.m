//
//  ISEditContactsViewController.m
//  infoSecure
//
//  Created by Amish Patel on 2013-04-10.
//  Copyright (c) 2013 Amish Patel. All rights reserved.
//

#import "ISEditContactsViewController.h"

@interface ISEditContactsViewController ()

@end

@implementation ISEditContactsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.firstNameTextField setText:[self.managedObject valueForKey:@"firstName"]];
    [self.lastNameTextField setText:[self.managedObject valueForKey:@"lastName"]];
    [self.cellTextField setText:[self.managedObject valueForKey:@"cellPhone"]];
    [self.homeTextField setText:[self.managedObject valueForKey:@"homePhone"]];
    [self.emailTextField setText:[self.managedObject valueForKey:@"email"]];
    [self.occupationTextField setText:[self.managedObject valueForKey:@"occupation"]];
    [self.addressTextField setText:[self.managedObject valueForKey:@"address"]];
    UIImage *image = [UIImage imageWithData:[self.managedObject valueForKey:@"picture"]];
    [self.imageView setImage:image];	// Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.scrollView setContentSize:CGSizeMake(320, 600)];
    [self.scrollView setScrollEnabled:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    
    id delegate = [[UIApplication sharedApplication]delegate];
    
    if ([delegate performSelector:@selector(managedObjectContext)]){
        context = [delegate managedObjectContext];
    }
    
    return context;
}

- (IBAction)donePressed:(UIBarButtonItem *)sender {
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    [self.managedObject setValue:[self.firstNameTextField text] forKey:@"firstName"];
    [self.managedObject setValue:[self.lastNameTextField text] forKey:@"lastName"];
    [self.managedObject setValue:[self.cellTextField text] forKey:@"cellPhone"];
    [self.managedObject setValue:[self.homeTextField text] forKey:@"homePhone"];
    [self.managedObject setValue:[self.emailTextField text] forKey:@"email"];
    [self.managedObject setValue:[self.occupationTextField text] forKey:@"occupation"];
    [self.managedObject setValue:[self.addressTextField text] forKey:@"address"];
    [self.managedObject setValue:UIImagePNGRepresentation(self.imageView.image) forKey:@"picture"];

    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}



- (IBAction)cancelPressed:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)pickImageFromCamera:(UIButton *)sender {
    self.picker = [[UIImagePickerController alloc]init];
    [self.picker setDelegate:self];
    [self.picker setSourceType:UIImagePickerControllerSourceTypeCamera];
    [self presentViewController:self.picker animated:YES completion:nil];
}

- (IBAction)pickImageFromGallary:(UIButton *)sender {
    self.picker2 = [[UIImagePickerController alloc]init];
    [self.picker2 setSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
    [self.picker2 setDelegate:self];
    [self presentViewController:self.picker2 animated:YES completion:nil];
}

//UIImagePickerDelegate Method
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    [self.imageView setImage:image];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
