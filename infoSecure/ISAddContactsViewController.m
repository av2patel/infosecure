//
//  ISAddContactsViewController.m
//  infoSecure
//
//  Created by Amish Patel on 2013-04-10.
//  Copyright (c) 2013 Amish Patel. All rights reserved.
//

#import "ISAddContactsViewController.h"

@interface ISAddContactsViewController ()

@end

@implementation ISAddContactsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    


	// Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.scrollView setContentSize:CGSizeMake(320, 600)];
    [self.scrollView setScrollEnabled:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//method that gives us the NSManagedObjectContext
- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication]delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}


//creates and saves the managed object
- (IBAction)donePressed:(id)sender {
    NSManagedObjectContext *context = [self managedObjectContext];
    if ([self.firstNameTextField.text length]>0){
        
        NSManagedObject *newContact = [NSEntityDescription insertNewObjectForEntityForName:@"Contacts" inManagedObjectContext:context];
        
        [newContact setValue:[self.firstNameTextField text] forKey:@"firstName"];
        [newContact setValue:[self.lastNameTextField text] forKey:@"lastName"];
        [newContact setValue:[self.cellTextField text] forKey:@"cellPhone"];
        [newContact setValue:[self.homePhoneTextField text] forKey:@"homePhone"];
        [newContact setValue:[self.emailTextField text] forKey:@"email"];
        [newContact setValue:[self.addressTextField text] forKey:@"address"];
        [newContact setValue:[self.occupationTextField text] forKey:@"occupation"];
        
        [newContact setValue:UIImagePNGRepresentation(self.imageView.image) forKey:@"picture"];
        
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
        
        NSError *error = nil;
        // Save the object to persistent store
        if (![context save:&error]) {
            NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        }
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"First Name Field is Required!" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles: nil];
        [alert show];
    }
    
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)cancelPressed:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}



//photo information


- (IBAction)pickImageFromCamera:(UIButton *)sender {
    self.picker = [[UIImagePickerController alloc]init];
    [self.picker setDelegate:self];
    [self.picker setSourceType:UIImagePickerControllerSourceTypeCamera];
    [self presentViewController:self.picker animated:YES completion:nil];
}

- (IBAction)pickImageFromGallary:(UIButton *)sender {
    self.picker2 = [[UIImagePickerController alloc]init];
    [self.picker2 setSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
    [self.picker2 setDelegate:self];
    [self presentViewController:self.picker2 animated:YES completion:nil];
}

//UIImagePickerDelegate Method
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    [self.imageView setImage:image];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
