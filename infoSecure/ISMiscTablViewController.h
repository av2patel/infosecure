//
//  ISMiscTablViewController.h
//  infoSecure
//
//  Created by Amish Patel on 2013-04-10.
//  Copyright (c) 2013 Amish Patel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>

@interface ISMiscTablViewController : UITableViewController

@property (strong, nonatomic) SLComposeViewController *mySLComposerSheet;

@end
