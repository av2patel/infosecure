//
//  Contacts.h
//  infoSecure
//
//  Created by Amish Patel on 2013-03-25.
//  Copyright (c) 2013 Amish Patel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Contacts : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * cellPhone;
@property (nonatomic, retain) NSString * firstName;
@property (nonatomic, retain) NSString * homePhone;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSData * picture;

@end
