//
//  ISContactsInfoViewController.m
//  infoSecure
//
//  Created by Amish Patel on 2013-04-10.
//  Copyright (c) 2013 Amish Patel. All rights reserved.
//

#import "ISContactsInfoViewController.h"

@interface ISContactsInfoViewController ()

@end

@implementation ISContactsInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    [self.firstNameLabel setText:[self.contactInfoObject valueForKey:@"firstName"]];
//    [self.lastNameLabel setText:[self.contactInfoObject valueForKey:@"lastName"]];
//    [self.cellLabel setText:[self.contactInfoObject valueForKey:@"cellPhone"]];
//    [self.homePhoneLabel setText:[self.contactInfoObject valueForKey:@"homePhone"]];
//    [self.addressLabel setText:[self.contactInfoObject valueForKey:@"address"]];
//    [self.emailLabel setText:[self.contactInfoObject valueForKey:@"email"]];
//    [self.occupationLabel setText:[self.contactInfoObject valueForKey:@"occupation"]];
	// Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.firstNameLabel setText:[self.contactInfoObject valueForKey:@"firstName"]];
    [self.lastNameLabel setText:[self.contactInfoObject valueForKey:@"lastName"]];
    [self.cellLabel setText:[self.contactInfoObject valueForKey:@"cellPhone"]];
    [self.homePhoneLabel setText:[self.contactInfoObject valueForKey:@"homePhone"]];
    [self.addressLabel setText:[self.contactInfoObject valueForKey:@"address"]];
    [self.emailLabel setText:[self.contactInfoObject valueForKey:@"email"]];
    [self.occupationLabel setText:[self.contactInfoObject valueForKey:@"occupation"]];
    UIImage *image = [UIImage imageWithData:[self.contactInfoObject valueForKey:@"picture"]];
    [self.imageView setImage:image];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    UINavigationController *nav = segue.destinationViewController;
    
    ISEditContactsViewController *editController = (ISEditContactsViewController *)nav.topViewController;
    editController.managedObject = self.contactInfoObject;

}


@end
