//
//  ISLoginViewController.m
//  infoSecure
//
//  Created by Amish Patel on 2013-03-25.
//  Copyright (c) 2013 Amish Patel. All rights reserved.
//

#import "ISLoginViewController.h"
#import "ISCoreDataHelper.h"


@interface ISLoginViewController ()

@end

@implementation ISLoginViewController

- (IBAction)resignAndLogin:(id)sender {

    if (sender == self.usernameField) {
        [self.passwordField becomeFirstResponder];
        
        //  Otherwise we pressed done on the password field, and want to attempt login
    } else {
        
        //  First put away the keyboard
        [sender resignFirstResponder];
        
        //  Set up a predicate (or search criteria) for checking the username and password
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"(username == %@ && password == %@)", [self.usernameField text], [self.passwordField text]];
        
        //  Actually run the query in Core Data and return the count of found users with these details
        //  Obviously if it found ANY then we got the username and password right!
        if ([ISCoreDataHelper countForEntity:@"Users" withPredicate:pred andContext:self.managedObjectContext] > 0)
        {
            //  We found a matching login user!  Force the segue transition to the next view
            [self performSegueWithIdentifier:@"LoginSegue" sender:sender];
        NSLog(@"called");
        }
        else
        {
            int num = [ISCoreDataHelper countForEntity:@"Users" withPredicate:pred andContext:self.managedObjectContext];
            NSLog(@"%i", num);
            //  We didn't find any matching login users. Wipe the password field to re-enter
            [self.passwordField setText:@""];

        }}}

- (void)viewWillAppear:(BOOL)animated {
    [self.usernameField setText:@""];
    [self.passwordField setText:@""];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
