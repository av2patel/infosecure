//
//  Contacts.m
//  infoSecure
//
//  Created by Amish Patel on 2013-03-25.
//  Copyright (c) 2013 Amish Patel. All rights reserved.
//

#import "Contacts.h"


@implementation Contacts

@dynamic address;
@dynamic cellPhone;
@dynamic firstName;
@dynamic homePhone;
@dynamic lastName;
@dynamic picture;

@end
