//
//  ISHomeScreenViewController.m
//  infoSecure
//
//  Created by Amish Patel on 2013-03-19.
//  Copyright (c) 2013 Amish Patel. All rights reserved.
//

#import "ISHomeScreenViewController.h"

@interface ISHomeScreenViewController ()

@end

@implementation ISHomeScreenViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.titles = [[NSArray alloc]initWithObjects:@"Contacts",@"Notes", @"Pictures", @"Things To DO", @"Websites", @"Youtube",nil];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //    the subsequent coding after this comment loads all the youtube videos and setting the text of the labels
    switch (indexPath.row) {
            
        case 0: {
            ISTableViewSeparatorCell *cell = [tableView dequeueReusableCellWithIdentifier:@"separatorCell"];
            return cell;
        }
        case 1: {
            ISContactsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"contactsCell"];
            [cell.imageView setImage:[UIImage imageNamed:@"contacts-icon.png"]];
            [cell.textLabel setText:@"Contacts"];
            return cell;
        }
        case 2: {
            ISNotesCell *cell = [tableView dequeueReusableCellWithIdentifier:@"notesCell"];
            [cell.imageView setImage:[UIImage imageNamed:@"Notes-icon.png"]];
            [cell.textLabel setText:@"Notes"];
            return cell;
        }
        case 3: {
            
            ISPicturesCell *cell = [tableView dequeueReusableCellWithIdentifier:@"picturesCell"];
            [cell.imageView setImage:[UIImage imageNamed:@"pictureIcon.png"]];
            [cell.textLabel setText:@"Pictures"];
            return cell;
        }
        case 4: {
            ISThingsToDoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"thingstoDoCell"];
            [cell.imageView setImage:[UIImage imageNamed:@"tasksIcon.png"]];
            [cell.textLabel setText:@"Things To Do"];
            return cell;
        }
        case 5: {
            
            ISWebsitesCell *cell = [tableView dequeueReusableCellWithIdentifier:@"websitesCell"];
            [cell.imageView setImage:[UIImage imageNamed:@"Location HTTP.png"]];
            [cell.textLabel setText:@"Websites"];
            return cell;
        }
        case 6: {
            ISYoutubeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"youtubeCell"];
            [cell.imageView setImage:[UIImage imageNamed:@"Media-youtube-icon.png"]];
            [cell.textLabel setText:@"Youtube"];
            return cell;
        }
        case 7: {
            ISMiscellaneousCell *cell = [tableView dequeueReusableCellWithIdentifier:@"miscCell"];
            [cell.textLabel setText:@"Miscallaneous"];
            return cell;
        }
            
        default:
            return nil;
    }
    

            
    
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        return 22;
    }
    else{
    return 44;
    }
}


@end
