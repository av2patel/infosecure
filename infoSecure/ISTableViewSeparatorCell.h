//
//  ISTableViewSeparatorCell.h
//  infoSecure
//
//  Created by Amish Patel on 2013-03-21.
//  Copyright (c) 2013 Amish Patel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ISTableViewSeparatorCell : UITableViewCell

@end
