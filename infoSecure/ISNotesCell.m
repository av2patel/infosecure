//
//  ISNotesCell.m
//  infoSecure
//
//  Created by Amish Patel on 2013-03-19.
//  Copyright (c) 2013 Amish Patel. All rights reserved.
//

#import "ISNotesCell.h"

@implementation ISNotesCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    if (selected) {
        [self setSelected:NO animated:YES];
    }
    // Configure the view for the selected state
}

@end
