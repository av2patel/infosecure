//
//  ISContactsViewController.h
//  infoSecure
//
//  Created by Amish Patel on 2013-04-10.
//  Copyright (c) 2013 Amish Patel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ISContactsNamesCell.h"
#import "ISContactsInfoViewController.h"


@interface ISContactsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, ISEditContactViewControllerDelegate>

@property (strong, nonatomic) NSMutableArray *contacts;
@property (weak, nonatomic) IBOutlet UITableView *tableView;



@end
