//
//  ISContactsInfoViewController.h
//  infoSecure
//
//  Created by Amish Patel on 2013-04-10.
//  Copyright (c) 2013 Amish Patel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ISEditContactsViewController.h"

@interface ISContactsInfoViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *firstNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *cellLabel;
@property (weak, nonatomic) IBOutlet UILabel *homePhoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *occupationLabel;
@property (weak, nonatomic) IBOutlet UITextView *addressLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) NSManagedObject *contactInfoObject;
@end
