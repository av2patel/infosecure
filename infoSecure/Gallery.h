//
//  Gallery.h
//  infoSecure
//
//  Created by Amish Patel on 2013-03-25.
//  Copyright (c) 2013 Amish Patel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Gallery : NSManagedObject

@property (nonatomic, retain) NSString * descripton;
@property (nonatomic, retain) NSData * image;
@property (nonatomic, retain) NSString * title;

@end
