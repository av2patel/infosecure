//
//  ThingsToDo.m
//  infoSecure
//
//  Created by Amish Patel on 2013-03-25.
//  Copyright (c) 2013 Amish Patel. All rights reserved.
//

#import "ThingsToDo.h"


@implementation ThingsToDo

@dynamic done;
@dynamic task;
@dynamic taskDescription;

@end
